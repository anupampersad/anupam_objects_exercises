const values = (obj) => {


    if(Object.prototype.toString.call(obj) !== '[object Object]'){
        return 'Pass an object as argument.'
    }

    let arr = []

    for (let i in obj) {

        if (typeof (obj[i]) == 'function') {
            continue
        }else{
            arr.push(obj[i])
        }

    }

    return arr
}

module.exports = values



