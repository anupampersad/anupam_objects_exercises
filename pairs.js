const pairs = (obj) => {

    if(Object.prototype.toString.call(obj) !== '[object Object]'){
        return 'Pass an object as argument.'
    }

    const arr = []
    for(i in obj){
        arr.push([i,obj[i]])
    }

    return arr
}


module.exports = pairs
