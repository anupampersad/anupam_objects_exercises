// If first argument passed is not an Object -> we get error
// If second argumnet is not an Object -> we get an error
// If second argument is not passed at all, first object is printed as it is


const defaults = (obj,default_obj) => {
    
    if(Object.prototype.toString.call(obj) !== '[object Object]'){
        return 'Pass an object as first argument.'
    }


    if(default_obj){

        if(Object.prototype.toString.call(default_obj) !== '[object Object]'){
            return 'Pass an object as a second argument.'
        }

        for(i in default_obj){
            if(i in obj){
                continue
            }else{
                obj[i] = default_obj[i]
            }
        }
    
        return obj

    }
    else{
        return obj
    }
    
}

module.exports = defaults