const invert = (obj) => {

    if(Object.prototype.toString.call(obj) !== '[object Object]'){
        return 'Pass an object as a parameter.'
    }

    const new_obj = {}

    for(i in obj){

        new_obj[obj[i]]= i
    }

    
    return new_obj

}

module.exports = invert


