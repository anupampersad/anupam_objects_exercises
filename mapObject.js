const mapObject = (object,cb) => {

    if (typeof(cb)!=='function'){
        return 'Please pass a call back function as second parameter'
        
    }

    if(Object.prototype.toString.call(object) !== '[object Object]'){
        return 'Pass an object as first parameter.'
    }

    new_obj = {}

    for(i in object){
        new_obj[i] = cb(object[i])
    }

    return new_obj
}


module.exports = mapObject

