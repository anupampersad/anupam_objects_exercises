const keys = (obj) => {


    if(Object.prototype.toString.call(obj) !== '[object Object]'){
        return 'Pass an object as a parameter.'
    }
    let arr = []

    for(let i in obj){
        arr.push(i)
    }

    return arr
}


module.exports = keys

