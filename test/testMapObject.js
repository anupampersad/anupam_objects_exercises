const mapObject = require('../mapObject')

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }
const testObject2 = {1:10, 2:20 , 3:30, 4:40};
let x = 9

const square = (value) => {
    return value*value
}

const type = (value) => {
    return typeof(value)
}

console.log(mapObject(testObject1,type))
console.log(mapObject(testObject2,square))
console.log(mapObject(testObject2,x))


